#include <stdio.h>

void my_sort_int_tab(int *tab, int size);

static void print_tab_(int *tab, int size)
{
  int i = 0;

  while (i < size)
    printf("%d ", tab[i++]);
}

int main(int argc, char const *argv[])
{
  int tab[3] = {1, 4, 2};
  int arg[5] = {19, 2, 1, -2, 5};
  int fake[2] = {1, 2};

  my_sort_int_tab(tab, 3);
  my_sort_int_tab(arg, 5);
  my_sort_int_tab(fake, -1);
  print_tab_(tab, 3);
  print_tab_(arg, 5);
  print_tab_(fake, 2);
	(void)argc;
	(void)argv;
	return 0;
}
