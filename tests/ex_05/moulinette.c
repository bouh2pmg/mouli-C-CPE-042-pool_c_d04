#include <stdio.h>

int my_getnbr(char *);

int main()
{
  printf("%d\n", my_getnbr("0"));
  printf("%d\n", my_getnbr("1000"));
  printf("%d\n", my_getnbr("-1000"));
  printf("%d\n", my_getnbr("42a42"));
  printf("%d\n", my_getnbr("-2147483648"));
  printf("%d\n", my_getnbr("2147483647"));
  printf("%d\n", my_getnbr("++++++++++---++-+-+-+-+0"));
  printf("%d\n", my_getnbr("++--+++--++---1337"));
  printf("%d\n", my_getnbr("123456789999999999999999"));
  printf("%d\n", my_getnbr("-123456789999999999999999"));
  return 0;
}
